#!/usr/bin/bash

. source.sh

workspace=$(mktemp -d)
shared_dir="$workspace/shared"
mkdir "$shared_dir"

trap 'rm -rf $workspace' EXIT ERR

cd "$workspace" || :

# Step: clone target repo and make changes
git clone "https://github.com/${TARGET_GH_REPO}.git" source/
cd source || :
sed -i -e "s/:1234/:${REVISION}/" deploy.yaml

# Step: record the updated files and the script will read them subsequently.
git status -s --porcelain | cut -c4- >"$workspace/shared/updated_files.txt"

echo "Updated files:"
cat "$workspace/shared/updated_files.txt"
echo

ls -l "$workspace/source"
echo
git status
echo
git diff

# Step: execute the script
export TARGET_REPO_SOURCE_DIR="$workspace/source"
export UPDATED_FILES_FILE="$workspace/shared/updated_files.txt"
cd "$shared_dir" || :
script="$1"
python3 "$script"

